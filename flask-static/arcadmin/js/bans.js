APP['bans'] = function (motdPlayer, serverclock, statusbar) {
    var bans = this;
    var nodes = {};
    nodes['bans-table'] = document.getElementById('bans-table');

    var banDurations = [];
    var bans = [];

    var renderBansTable = function () {
        clearNode(nodes['bans-table']);

        var tr, th, td;

        // Table header
        tr = nodes['bans-table'].appendChild(document.createElement('tr'));

        th = tr.appendChild(document.createElement('th'));
        th.appendChild(document.createTextNode("Status"));
        th.classList.add('bans-table-hdr-status');

        th = tr.appendChild(document.createElement('th'));
        th.appendChild(document.createTextNode("SteamID"));
        th.classList.add('bans-table-hdr-steamid');

        th = tr.appendChild(document.createElement('th'));
        th.appendChild(document.createTextNode("Name"));
        th.classList.add('bans-table-hdr-name');

        th = tr.appendChild(document.createElement('th'));
        th.appendChild(document.createTextNode("Ban Date/Time"));
        th.classList.add('bans-table-hdr-banned');

        th = tr.appendChild(document.createElement('th'));
        th.appendChild(document.createTextNode("Expiration Date/Time"));
        th.classList.add('bans-table-hdr-expires');

        th = tr.appendChild(document.createElement('th'));
        th.appendChild(document.createTextNode("Reason"));
        th.classList.add('bans-table-hdr-reason');

        th = tr.appendChild(document.createElement('th'));
        th.appendChild(document.createTextNode("Confirm ban / Unban"));
        th.classList.add('bans-table-hdr-save');

        // Table content
        for (var i = 0; i < bans.length; i++) {
            (function (ban) {
                tr = nodes['bans-table'].appendChild(document.createElement('tr'));

                td = tr.appendChild(document.createElement('td'));
                td.appendChild(document.createTextNode(ban['status']));
                td.classList.add('bans-table-content-status');

                td = tr.appendChild(document.createElement('td'));
                td.appendChild(document.createTextNode(ban['steamid']));
                td.classList.add('bans-table-content-steamid');

                td = tr.appendChild(document.createElement('td'));
                td.appendChild(document.createTextNode(ban['name']));
                td.classList.add('bans-table-content-name');

                td = tr.appendChild(document.createElement('td'));
                td.appendChild(document.createTextNode(ban['banned']));
                td.classList.add('bans-table-content-banned');

                td = tr.appendChild(document.createElement('td'));
                td.classList.add('bans-table-content-expires');
                if (ban['status'] != "NEW") {
                    if (ban['expires'] < 0)
                        td.appendChild(document.createTextNode("N/A"));
                    else
                        td.appendChild(document.createTextNode(ban['expires']));
                }
                else {
                    var option, selectDuration = td.appendChild(document.createElement('select'));

                    for (var j = 0; j < banDurations.length; j++) {
                        option = selectDuration.appendChild(document.createElement('option'));
                        option.appendChild(document.createTextNode(banDurations[j].text));
                        option.value = banDurations[j].value;
                    }
                }

                td = tr.appendChild(document.createElement('td'));
                td.classList.add('bans-table-content-reason');
                if (ban['status'] != "NEW") {
                    if (ban['reason'])
                        td.appendChild(document.createTextNode(ban['reason']));
                    else {
                        td.appendChild(document.createTextNode("none specified"));
                        td.classList.add('bans-table-content-reason-blank')
                    }
                }
                else {
                    var textareaReason = td.appendChild(document.createElement('textarea'));
                }

                td = tr.appendChild(document.createElement('td'));
                td.classList.add('bans-table-content-save');
                if (ban['status'] == "ACTIVE") {
                    td.appendChild(document.createTextNode("Already reviewed"));
                }
                else if (ban['status'] == "EXPIRED") {
                    td.appendChild(document.createTextNode("Already expired"));
                }
                else if (ban['status'] == "UNBANNED") {
                    td.appendChild(document.createTextNode("Already unbanned"));
                }
                else {
                    var button1, button2;

                    button1 = td.appendChild(document.createElement('input'));
                    button1.type = "button";
                    button1.value = "Confirm ban";
                    button1.addEventListener('click', function (e) {
                        if (textareaReason.value.trim() == "") {
                            alert("Please fill the reason field");
                            return;
                        }
                        motdPlayer.post({
                            action: "confirm-ban",
                            ban_id: ban['id'],
                            duration: selectDuration.value,
                            reason: textareaReason.value,
                        }, function (data) {
                            handleResponseData(data);
                        }, function (error) {
                            alert("Communication Error:\n" + error);
                        });
                    });

                    td.appendChild(document.createElement('br'));

                    button2 = td.appendChild(document.createElement('input'));
                    button2.type = "button";
                    button2.value = "Unban";
                    button2.addEventListener('click', function (e) {
                        motdPlayer.post({
                            action: "unban",
                            ban_id: ban['id'],
                        }, function (data) {
                            handleResponseData(data);
                        }, function (error) {
                            alert("Communication Error:\n" + error);
                        });
                    });
                }
            })(bans[i]);
        }
    }

    var handleResponseData = function (data) {
        if (data['error']) {
            alert(data['error']);
            return;
        }
        serverclock.timestamp = data['server_time'];

        statusbar.version = data['version'];
        statusbar.requests++;
        statusbar.render();

        banDurations = data['ban_durations'];
        bans = data['bans'];
        renderBansTable();
    }

    motdPlayer.retarget('json-bans', function () {
        motdPlayer.post({
            action: "init",
        }, function (data) {
            handleResponseData(data);
        }, function (error) {
            alert("Initialization Error\n" + error);
        });
    }, function (error) {
        alert("Retargeting Error\n" + error);
    });
};