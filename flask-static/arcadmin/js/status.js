APP['statusbar'] = function (node, STEAMID) {
    var statusbar = this;

    this.version = "unknown";
    this.requests = 0;

    this.render = function () {
        clearNode(node);

        text = "ArcAdmin v" + statusbar.version + " by Kirill (iPlayer) Mysnik • ";
        text += "Your Steam community ID: " + STEAMID + " • ";
        text += "AJAX requests sent by this page: " + statusbar.requests;
        node.appendChild(document.createTextNode(text));
    }
};
