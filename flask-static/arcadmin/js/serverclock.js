APP['serverclock'] = function (node) {
    var serverclock = this;

    this.timestamp = 0;

    var padded = function (value) {
        return (value < 10 ? "0" : "") + value;
    }

    var formatTimestamp = function (ts) {
        var date = new Date(ts * 1000);

        var day = padded(date.getDate());
        var month = padded(date.getMonth() + 1);
        var year = date.getFullYear();
        var hour = padded(date.getHours());
        var minute = padded(date.getMinutes());
        var second = padded(date.getSeconds());

        return day + "." + month + "." + year + " " + hour + ":" + minute + ":" + second;
    }

    var render = function () {
        clearNode(node);

        node.appendChild(document.createTextNode("server time: " + formatTimestamp(serverclock.timestamp)));
    }

    var interval = setInterval(function () {
        serverclock.timestamp++;
        render();
    }, 1000);
};
