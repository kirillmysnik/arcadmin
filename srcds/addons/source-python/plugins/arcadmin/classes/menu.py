from filters.players import PlayerIter
from menus import PagedMenu, PagedOption
from menus.radio import (
    BUTTON_BACK, BUTTON_NEXT, BUTTON_CLOSE, BUTTON_CLOSE_SLOT,
    PagedRadioOption)

from .client import client_manager, tell
from .returntype import ReturnType

from ..resource.strings import strings_common, strings_popups


class PagedSubmenu(PagedMenu):
    def __init__(
            self, data=None, select_callback=None,
            build_callback=None, description=None,
            title=None, top_separator='-' * 30, bottom_separator='-' * 30,
            fill=True, parent_callback=None):

        super().__init__(data, select_callback, build_callback, description,
                         title, top_separator, bottom_separator, fill)

        self.parent_callback = parent_callback

    def _format_footer(self, player_index, page, slots):
        buffer = ''

        # Set the bottom separator if present
        if self.bottom_separator is not None:
            buffer += self.bottom_separator + '\n'

        # Add "Back" option
        back_selectable = page.index > 0 or self.parent_callback is not None
        buffer += PagedRadioOption(
            strings_popups['back'], highlight=back_selectable)._render(
                player_index, BUTTON_BACK)
        if back_selectable:
            slots.add(BUTTON_BACK)

        # Add "Next" option
        next_selectable = page.index < self.last_page_index
        buffer += PagedRadioOption(
            strings_popups['next'], highlight=next_selectable)._render(
                player_index, BUTTON_NEXT)
        if next_selectable:
            slots.add(BUTTON_NEXT)

        # Add "Close" option
        buffer += PagedRadioOption(
            strings_popups['close'], highlight=False)._render(
                player_index, BUTTON_CLOSE_SLOT)

        # Return the buffer
        return buffer

    def _select(self, player_index, choice_index):
        # Do nothing if the menu is being closed
        if choice_index == BUTTON_CLOSE:
            del self._player_pages[player_index]
            return None

        # Get the player's current page
        page = self._player_pages[player_index]

        # Display previous page?
        if choice_index == BUTTON_BACK:
            if page.index > 0:
                self.set_player_page(player_index, page.index - 1)
                return self

            if self.parent_callback is not None:
                return self.parent_callback(player_index)

            return None

        # Display next page?
        if choice_index == BUTTON_NEXT:
            self.set_player_page(player_index, page.index + 1)
            return self

        return super()._select(player_index, choice_index)


class MenuItem:
    def __init__(self, parent, title, flag=None, id_=None):
        super().__init__()

        self._parent = parent
        self.title = title
        self.flag = flag
        self.id = id_

    def is_visible(self, client):
        if (self.flag is not None and
                    client.get_permission_level(self.flag) < 0):

            return False

        return ReturnType.CONTINUE

    def is_active(self, client):
        return ReturnType.CONTINUE

    def select_callback(self, client):
        if not self.is_visible(client) or not self.is_active(client):
            tell(client, strings_common['unavailable'])
            return ReturnType.RETURN

        return ReturnType.CONTINUE


class Section(MenuItem, list):
    def __init__(self, parent, title, flag=None, id_=None):
        super().__init__(parent, title, flag, id_)

        self.order = []

    def is_visible(self, client):
        rs = super().is_visible(client)
        if rs != ReturnType.CONTINUE:
            return None if rs == ReturnType.RETURN else rs

        for section in self:
            if section.is_visible(client):
                return True

        return False

    def is_active(self, client):
        rs = super().is_visible(client)
        if rs != ReturnType.CONTINUE:
            return None if rs == ReturnType.RETURN else rs

        for section in self:
            if section.is_active(client):
                return True

        return False

    def select_callback(self, client):
        if super().select_callback(client) is ReturnType.RETURN:
            return

        self.show_popup(client)

    def add_child(self, class_, title, flag=None, id_=None, **kwargs):
        item = class_(self, title, flag, id_, **kwargs)
        self.append(item)
        self.sort(key=lambda item: (self.order.index(item.id)
                                    if item.id in self.order else -1))

        return item

    def build_popup(self, client):
        def select_callback(popup, index, option):
            option.value.select_callback(client_manager[index])

        parent_callback = (None if self._parent is None else
                           self._parent.build_popup_for_index)

        popup = PagedSubmenu(select_callback=select_callback,
                             title=self.title,
                             parent_callback=parent_callback)

        for item in self:
            if not item.is_visible(client):
                continue

            selectable = item.is_active(client)
            if isinstance(item, Section):
                title = strings_popups['extend'].tokenize(
                    item=item.title)
            else:
                title = item.title

            popup.append(PagedOption(
                text=title,
                value=item,
                highlight=selectable,
                selectable=selectable
            ))

        return popup

    def show_popup(self, client):
        popup = self.build_popup(client)
        client.send_popup(popup)

    def build_popup_for_index(self, index):
        return self.build_popup(client_manager[index])

popup_main = Section(None, strings_popups['title main'], 'admin')


class Command(MenuItem):
    def select_callback(self, client):
        raise NotImplementedError


class PlayerBasedCommand(Command):
    base_filter = 'all'
    include_self = True
    include_equal_priorities = False
    allow_multiple_choices = True

    class SelectionFrame:
        client = None
        players = None
        selecting_multiple = False
        special_toggle_multiple = False

    def filter(self, client, player):
        if not self.include_self and client.player == player:
            return False

        another_client = client_manager[player.index]
        if self.include_equal_priorities:
            if (client.get_permission_level(self.flag) <
                    another_client.get_permission_level(self.flag)):

                return False
        else:
            if (client.player != player and
                    client.get_permission_level(self.flag) <=
                    another_client.get_permission_level(self.flag)):

                return False

        return True

    @staticmethod
    def player_name(player):
        return player.name

    def select_callback(self, client):
        parent_callback = (None if self._parent is None else
                           self._parent.build_popup_for_index)

        if self.allow_multiple_choices:
            def player_select_callback(popup, index, option):
                frame = option.value

                selecting_multiple = frame.selecting_multiple
                if frame.special_toggle_multiple:
                    if frame.selecting_multiple:
                        if frame.players:
                            self.player_select_callback(
                                frame.client, frame.players)

                            return

                        selecting_multiple = False

                    else:
                        selecting_multiple = True

                elif not frame.selecting_multiple:
                    self.player_select_callback(frame.client, frame.players)
                    return

                string = strings_popups[
                    'title select_players' if selecting_multiple else
                    'title select_player']

                popup = PagedSubmenu(select_callback=player_select_callback,
                                     title=string.tokenize(base=self.title),
                                     parent_callback=parent_callback)

                selected_players = list(filter(
                    lambda player: self.filter(client, player), frame.players))

                # From now on, 'frame' is no longer a frame we've received -
                # - it's a local var used by new frames

                if selecting_multiple:
                    for player in PlayerIter(self.base_filter):
                        if not self.filter(client, player):
                            continue

                        for player_ in selected_players:
                            if player_ == player:
                                new_players = list(filter(
                                    lambda player__: player__ != player,
                                    selected_players))

                                string = strings_popups[
                                    'select_player selected']

                                break
                        else:
                            new_players = selected_players + [player, ]
                            string = strings_popups['select_player unselected']

                        frame = self.SelectionFrame()
                        frame.client = client
                        frame.players = new_players
                        frame.selecting_multiple = True
                        frame.special_toggle_multiple = False

                        popup.append(PagedOption(
                            text=string.tokenize(
                                base=self.player_name(player)
                            ),
                            value=frame,
                        ))

                    frame = self.SelectionFrame()
                    frame.client = client
                    frame.players = selected_players
                    frame.selecting_multiple = True
                    frame.special_toggle_multiple = True

                    string = (
                        strings_popups['select_player select_selected']
                        if selected_players else
                        strings_popups['select_player toggle_selection_off']
                    )

                    popup.insert(0, PagedOption(text=string, value=frame))

                else:
                    frame = self.SelectionFrame()
                    frame.client = client
                    frame.players = selected_players
                    frame.selecting_multiple = False
                    frame.special_toggle_multiple = True

                    popup.append(PagedOption(
                        text=strings_popups[
                                  'select_player toggle_selection_on'],
                        value=frame,
                    ))

                    for player in PlayerIter(self.base_filter):
                        if not self.filter(client, player):
                            continue

                        new_players = selected_players + [player, ]
                        string = strings_popups['select_player single']

                        frame = self.SelectionFrame()
                        frame.client = client
                        frame.players = new_players
                        frame.selecting_multiple = False
                        frame.special_toggle_multiple = False

                        popup.append(PagedOption(
                            text=string.tokenize(
                                base=self.player_name(player)
                            ),
                            value=frame,
                        ))

                client.send_popup(popup)

            popup = PagedSubmenu(
                select_callback=player_select_callback,
                title=strings_popups['title select_player'].tokenize(
                    base=self.title
                ),
                parent_callback=parent_callback,
            )

            frame = self.SelectionFrame()
            frame.client = client
            frame.players = []
            frame.selecting_multiple = False
            frame.special_toggle_multiple = True

            popup.append(PagedOption(
                text=strings_popups['select_player toggle_selection_on'],
                value=frame,
            ))

            for player in PlayerIter(self.base_filter):
                if not self.filter(client, player):
                    continue

                string = strings_popups['select_player single']

                frame = self.SelectionFrame()
                frame.client = client
                frame.players = [player, ]
                frame.selecting_multiple = False
                frame.special_toggle_multiple = False

                popup.append(PagedOption(
                    text=string.tokenize(base=self.player_name(player)),
                    value=frame,
                ))

            client.send_popup(popup)

        else:
            def player_select_callback(popup, index, option):
                frame = option.value
                self.player_select_callback(frame.client, frame.players)

            popup = PagedSubmenu(
                select_callback=player_select_callback,
                title=strings_popups['title select_player'].tokenize(
                    base=self.title
                ),
                parent_callback=parent_callback,
            )

            for player in PlayerIter(self.base_filter):
                if not self.filter(client, player):
                    continue

                frame = self.SelectionFrame()
                frame.client = client
                frame.players = [player, ]
                frame.selecting_multiple = False
                frame.special_toggle_multiple = False

                popup.append(PagedOption(
                    text=self.player_name(player),
                    value=frame,
                ))

            client.send_popup(popup)

    def player_select_callback(self, client, players):
        raise NotImplementedError
