from enum import IntEnum


class ReturnType(IntEnum):
    RETURN = 0
    CONTINUE = 1
