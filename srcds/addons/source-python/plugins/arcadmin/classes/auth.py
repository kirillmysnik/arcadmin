from ..internal_events import InternalEvent


class Permission:
    def __init__(self, flag, level):
        self.flag = flag
        self._flag_split = flag.split('.')
        self.level = level

    def __hash__(self):
        return hash(self.flag)

    def matches(self, flag):
        flag_split = flag.split('.')

        for i in range(len(flag_split)):
            if i >= len(self._flag_split):
                return False

            if self._flag_split[i] == '*':
                return True

            if self._flag_split[i] != flag_split[i]:
                return False

        return True


class BaseAuthProvider:
    def get_permission_level(self, client, flag):
        raise NotImplementedError

    def reload_admins(self):
        raise NotImplementedError


class AuthManager(dict):
    def add_provider(self, id_, provider):
        if id_ in self:
            raise ValueError("Provider '{}' is already registered".format(id_))

        self[id_] = provider

    def remove_provider(self, id_):
        del self[id_]

    def get_permission_level(self, client, flag):
        max_ = -1
        for auth_provider in self.values():
            max_ = max(auth_provider.get_permission_level(client, flag), max_)
        return max_

    def get_permissions(self, client):
        result = set()
        for auth_provider in self.values():
            result.update(auth_provider.get_permissions(client))
        return result

    def reload_admins(self):
        for auth_provider in self.values():
            auth_provider.reload_admins()

auth_manager = AuthManager()


@InternalEvent('load')
def on_load(event_var):
    auth_manager.reload_admins()
