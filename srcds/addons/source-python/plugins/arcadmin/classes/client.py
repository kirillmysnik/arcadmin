from messages import SayText2
from players.dictionary import PlayerDictionary
from players.entity import Player

from ..resource.strings import COLOR_SCHEME, strings_common

from .auth import auth_manager


class Client:
    def __init__(self, index):
        self.player = Player(index)

        self._active_popup = None

    def get_permission_level(self, flag):
        return auth_manager.get_permission_level(self, flag)

    def send_popup(self, popup):
        if self._active_popup is not None:
            self._active_popup.close(self.player.index)

        self._active_popup = popup
        popup.send(self.player.index)

    def announce(self, message, **tokens):
        if self.get_permission_level('anonymity') < 0:
            message = strings_common['action_announce'].tokenize(
                client=self.player.name,
                message=message.tokenize(**COLOR_SCHEME),
            )
        broadcast(message, **tokens)

client_manager = PlayerDictionary(Client)


def tell(clients, message, **tokens):
    """Send a SayText2 message to a list of Client instances."""
    if isinstance(clients, Client):
        clients = (clients, )

    player_indexes = [client.player.index for client in clients]

    tokens.update(COLOR_SCHEME)

    message = message.tokenize(**tokens)
    message = strings_common['chat_base'].tokenize(
        message=message, **COLOR_SCHEME)

    SayText2(message=message).send(*player_indexes)


def broadcast(message, **tokens):
    """Send a SayText2 message to all registered users."""
    tell(list(client_manager.values()), message, **tokens)
