from datetime import datetime
import json
from time import time

from spam_proof_commands.client import ClientCommand
from spam_proof_commands.say import SayCommand

from ...classes.client import client_manager, tell
from ...info import info
from ...models.banned_user import BannedUser as DB_BannedUser
from ...modules.bans import banned_user_database
from ...resource.paths import ARCADMIN_DATA_PATH
from ...resource.sqlalchemy import Session
from ...resource.strings import build_module_strings

from . import plugin_instance


CMD_ANTISPAM_TIMEOUT = 2


strings_module = build_module_strings('motd.bans')

with open(ARCADMIN_DATA_PATH / 'ban-durations.json') as f:
    ban_durations = json.load(f)


class InvalidRequestError(Exception):
    pass


def format_ts(ts):
    return datetime.fromtimestamp(ts).strftime('%d.%m.%Y %H:%M:%S')


def send_page(client):
    def bans_callback(data, error):
        pass

    def json_bans_callback(data, error):
        if error is not None:
            return

        current_time = time()
        db_session = Session()

        if data['action'] in ("confirm-ban", "unban"):
            db_banned_user = db_session.query(
                DB_BannedUser).filter_by(id=data['ban_id']).first()

            try:
                if db_banned_user.admin_steamid != client.player.steamid:
                    raise InvalidRequestError("APPERR_WRONG_ADMIN")

                if db_banned_user.reviewed:
                    raise InvalidRequestError("APPERR_ALREADY_REVIEWED")

                if db_banned_user.unbanned:
                    raise InvalidRequestError("APPERR_ALREADY_UNBANNED")

                if -1 < db_banned_user.expires_timestamp < current_time:
                    raise InvalidRequestError("APPERR_ALREADY_EXPIRED")

                if data['action'] == "confirm-ban":

                    # Confirm ban, fill in duration and reason

                    # TODO: Implement duration check

                    db_banned_user.reason = data['reason']

                    if data['duration'] > -1:
                        expiration_time = (
                            db_banned_user.banned_timestamp + data['duration'])

                        db_banned_user.expires_timestamp = expiration_time

                        banned_user_database[db_banned_user.steamid] = \
                            expiration_time

                    else:
                        db_banned_user.expires_timestamp = -1
                        banned_user_database[db_banned_user.steamid] = None

                else:

                    # Unban player
                    db_banned_user.unbanned = True

                    if db_banned_user.steamid in banned_user_database:
                        del banned_user_database[db_banned_user.steamid]

                db_banned_user.reviewed = True
                db_session.commit()

            except InvalidRequestError as e:
                db_session.close()
                return {'error': e}

        bans = []

        db_banned_users = db_session.query(DB_BannedUser).\
            filter_by(admin_steamid=client.player.steamid).\
            order_by(DB_BannedUser.id.desc())

        for db_banned_user in db_banned_users:
            if db_banned_user.unbanned:
                status = "UNBANNED"
            else:
                if db_banned_user.expires_timestamp < current_time:
                    status = "EXPIRED"
                else:
                    if db_banned_user.reviewed:
                        status = "ACTIVE"
                    else:
                        status = "NEW"

            bans.append({
                'id': db_banned_user.id,
                'status': status,
                'steamid': db_banned_user.steamid,
                'name': db_banned_user.name,
                'banned': format_ts(db_banned_user.banned_timestamp),
                'expires': format_ts(db_banned_user.expires_timestamp),
                'reason': db_banned_user.reason,
                'reviewed': db_banned_user.reviewed,
            })

        db_session.close()

        return {
            'bans': bans,
            'ban_durations': ban_durations,
            'server_time': current_time,
            'version': info.version,
        }

    def bans_retargeting_callback(new_page_id):
        if new_page_id == "json-bans":
            return json_bans_callback, json_bans_retargeting_callback

    def json_bans_retargeting_callback(new_page_id):
        return None

    plugin_instance.send_page(
        client.player, 'bans', bans_callback, bans_retargeting_callback)


@SayCommand(CMD_ANTISPAM_TIMEOUT, '!bans')
@ClientCommand(CMD_ANTISPAM_TIMEOUT, 'bans')
def say_bans(command, index, team_only=None):
    client = client_manager[index]
    if client.get_permission_level('admin.bans.ban') > 0:
        send_page(client)
    else:
        tell(client, strings_module['access_denied'])
