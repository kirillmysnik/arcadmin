from motdplayer.plugin_instance import PluginInstance

from ...info import info

plugin_instance = PluginInstance(info.basename)


import os

from .. import parse_modules


current_dir = os.path.dirname(__file__)
__all__ = parse_modules(current_dir)


from . import *
