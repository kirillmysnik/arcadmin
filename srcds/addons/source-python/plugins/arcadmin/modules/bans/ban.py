from time import time

from listeners.tick import GameThread
from players.helpers import get_client_language

from ...classes.menu import PlayerBasedCommand

from ...models.banned_user import BannedUser as DB_BannedUser

from ...resource.config import config
from ...resource.sqlalchemy import Session
from ...resource.strings import build_module_strings

from . import banned_user_database, section


strings_module = build_module_strings('bans.ban')


def save_ban_to_database(client, banned_steamid, banned_name, duration):
    db_session = Session()

    db_banned_user = DB_BannedUser()
    db_session.add(db_banned_user)

    current_time = time()
    db_banned_user.steamid = banned_steamid
    db_banned_user.name = banned_name
    db_banned_user.admin_steamid = client.player.steamid
    db_banned_user.reviewed = False
    db_banned_user.banned_timestamp = current_time
    db_banned_user.expires_timestamp = current_time + duration
    db_banned_user.unbanned = False
    db_banned_user.reason = ""
    db_banned_user.notes = ""

    db_session.commit()
    db_session.close()


class BanCommand(PlayerBasedCommand):
    base_filter = 'human'
    allow_multiple_choices = False
    include_equal_priorities = False
    include_self = False

    def player_select_callback(self, client, players):
        for player in players:
            banned_steamid, banned_name = player.steamid, player.name
            language = get_client_language(player.index)
            player.kick(strings_module['reason'].get_string(language))

            current_time = time()
            duration = int(config['settings']['default_ban_time_seconds'])

            banned_user_database[banned_steamid] = current_time + duration

            GameThread(
                target=save_ban_to_database,
                args=(client, banned_steamid, banned_name, duration)
            ).start()

section.add_child(
    BanCommand, strings_module['popup title'], 'admin.bans.ban', 'ban')
