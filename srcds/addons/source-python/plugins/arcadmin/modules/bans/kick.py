from players.helpers import get_client_language

from ...classes.menu import PlayerBasedCommand

from ...resource.strings import build_module_strings

from . import section


strings_module = build_module_strings('bans.kick')


class KickCommand(PlayerBasedCommand):
    base_filter = 'human'
    allow_multiple_choices = False
    include_equal_priorities = False
    include_self = False

    def player_select_callback(self, client, players):
        for player in players:
            language = get_client_language(player.index)
            player.kick(strings_module['reason'].get_string(language))

section.add_child(
    KickCommand, strings_module['popup title'], 'admin.bans.kick', 'kick')
