from time import time

from core import GAME_NAME
from engines.server import server
from listeners import OnClientConnect, OnNetworkidValidated
from memory import Convention, DataType, make_object
from memory.hooks import PostHook
from players import Client
from translations.manager import language_manager

from ...internal_events import InternalEvent

from ...models.banned_user import BannedUser as DB_BannedUser

from ...resource.memory import custom_server
from ...resource.sqlalchemy import Session
from ...resource.strings import build_module_strings

from ...classes.menu import popup_main
from ...classes.menu import Section


strings_module = build_module_strings('bans')
section = popup_main.add_child(Section, strings_module['popup title'])


def find_client(steamid):
    for x in range(server.num_clients):
        client = server.get_client(x)
        if client.steamid == steamid:
            return client

    return None


class BannedUserDatabase(dict):
    def refresh(self):
        self.clear()

        db_session = Session()

        db_banned_users = db_session.query(DB_BannedUser).all()

        current_time = time()
        for db_banned_user in db_banned_users:
            if db_banned_user.unbanned:
                continue

            if -1 < db_banned_user.expires_timestamp < current_time:
                continue

            if db_banned_user.expires_timestamp < 0:
                self[db_banned_user.steamid] = None
            else:
                self[db_banned_user.steamid] = db_banned_user.expires_timestamp

        db_session.close()

    def is_steamid_banned(self, steamid):
        if steamid not in self:
            return False

        if self[steamid] is None:
            return True

        current_time = time()
        if self[steamid] < current_time:
            del self[steamid]
            return False

        return True

banned_user_database = BannedUserDatabase()


@OnNetworkidValidated
def listener_on_networkid_validated(name, steamid):
    if not banned_user_database.is_steamid_banned(steamid):
        return

    client = find_client(steamid)
    if client is None:
        return

    client.disconnect(
        strings_module['reason'].get_string(language_manager.default))


@PostHook(custom_server.check_challenge_type)
def post_check_challenge_type(args, return_value=0):
    client = make_object(Client, args[1] + 4)
    if not banned_user_database.is_steamid_banned(client.steamid):
        return

    if GAME_NAME == 'csgo':
        custom_server.reject_connection(
            args[3],
            strings_module['reason'].get_string(language_manager.default)
        )
    else:
        custom_server.reject_connection(
            args[3], args[7],
            strings_module['reason'].get_string(language_manager.default)
        )

    return False


class BannedIPDatabase(dict):
    pass


@OnClientConnect
def listener_on_client_connect(
        allow_connect, edict, name, address, reject_msg, reject_msg_len):

    pass


@InternalEvent('load')
def on_load(event_var):
    banned_user_database.refresh()


import os

from .. import parse_modules


__all__ = parse_modules(os.path.dirname(__file__))

from . import *
