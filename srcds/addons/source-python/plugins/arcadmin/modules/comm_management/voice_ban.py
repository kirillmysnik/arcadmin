from time import time

from listeners import OnClientActive
from listeners.tick import GameThread
from players.voice import mute_manager

from ...classes.client import client_manager
from ...classes.menu import PlayerBasedCommand
from ...models.voice_banned_user import VoiceBannedUser as DB_VoiceBannedUser
from ...resource.sqlalchemy import Session
from ...resource.strings import build_module_strings

from . import voice_banned_user_database, section


strings_module = build_module_strings('comm_management.voice_ban')


def save_ban_to_database(client, player):
    db_session = Session()

    db_voice_banned_user = DB_VoiceBannedUser()
    db_session.add(db_voice_banned_user)

    db_voice_banned_user.steamid = player.steamid
    db_voice_banned_user.name = player.name
    db_voice_banned_user.admin_steamid = client.player.steamid
    db_voice_banned_user.banned_timestamp = time()
    db_voice_banned_user.unbanned = False

    db_session.commit()
    db_session.close()


def save_unban_to_database(client, player):
    db_session = Session()

    db_voice_banned_user = db_session.query(
        DB_VoiceBannedUser).filter_by(steamid=player.steamid).first()

    if db_voice_banned_user is None:
        return

    db_voice_banned_user.unbanned = True

    db_session.commit()
    db_session.close()


def toggle_ban(client, player):
    if player.steamid in voice_banned_user_database:
        voice_banned_user_database.remove(player.steamid)
        mute_manager.unmute_player(player.index)
        client.announce(strings_module['unbanned'].tokenize(
            player=player.name,
        ))
        GameThread(
            target=save_unban_to_database,
            args=(client, player)
        ).start()

    else:
        voice_banned_user_database.add(player.steamid)
        mute_manager.mute_player(player.index)
        client.announce(strings_module['banned'].tokenize(
            player=player.name,
        ))
        GameThread(
            target=save_ban_to_database,
            args=(client, player)
        ).start()


class VoiceBanCommand(PlayerBasedCommand):
    base_filter = 'human'
    include_equal_priorities = False
    include_self = False

    @staticmethod
    def player_name(player):
        if player.steamid in voice_banned_user_database:
            return strings_module['popup prefix'].tokenize(base=player.name)
        return player.name

    def player_select_callback(self, client, players):
        for player in players:
            toggle_ban(client, player)

section.add_child(VoiceBanCommand, strings_module['popup title'],
                  'admin.comm_management.voice_ban', 'voice_ban')


@OnClientActive
def listener_on_client_active(index):
    client = client_manager[index]
    if client.player.steamid in voice_banned_user_database:
        mute_manager.mute_player(index)
