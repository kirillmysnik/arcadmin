from listeners import OnClientDisconnect
from players.voice import mute_manager

from ...classes.client import tell
from ...classes.menu import PlayerBasedCommand
from ...resource.strings import build_module_strings

from . import section, voice_banned_user_database


strings_module = build_module_strings('comm_management.voice_mute')

# We store muted players manually, we don't use mute_manager.is_muted -
# because we don't want to unmute voice-banned players
temp_muted_players = set()


def toggle_mute(client, player):
    if player.steamid in voice_banned_user_database:
        tell(client, strings_module['error already_voice_banned'])
        return

    if player.index in temp_muted_players:
        temp_muted_players.remove(player.index)
        mute_manager.unmute_player(player.index)
        client.announce(strings_module['unmuted'].tokenize(
            player=player.name,
        ))

    else:
        temp_muted_players.add(player.index)
        mute_manager.mute_player(player.index)
        client.announce(strings_module['muted'].tokenize(
            player=player.name,
        ))


class MuteCommand(PlayerBasedCommand):
    include_equal_priorities = False
    include_self = False

    def filter(self, client, player):
        default = super().filter(client, player)
        if not default:
            return False

        return player.steamid not in voice_banned_user_database

    @staticmethod
    def player_name(player):
        if player.index in temp_muted_players:
            return strings_module['popup prefix'].tokenize(base=player.name)
        return player.name

    def player_select_callback(self, client, players):
        for player in players:
            toggle_mute(client, player)

section.add_child(MuteCommand, strings_module['popup title'],
                  'admin.comm_management.voice_mute', 'voice_mute')


@OnClientDisconnect
def listener_on_client_disconnect(index):
    temp_muted_players.discard(index)
