from events import Event

from ...classes.client import client_manager, tell
from ...classes.menu import popup_main, Section
from ...internal_events import InternalEvent
from ...models.text_banned_user import TextBannedUser as DB_TextBannedUser
from ...models.voice_banned_user import VoiceBannedUser as DB_VoiceBannedUser
from ...resource.sqlalchemy import Session
from ...resource.strings import build_module_strings


strings_module = build_module_strings('comm_management')
section = popup_main.add_child(Section, strings_module['popup title'])


class CommBannedUserDatabase(set):
    db_class = None

    def refresh(self):
        self.clear()

        db_session = Session()
        db_comm_banned_users = db_session.query(self.db_class).all()

        for db_comm_banned_user in db_comm_banned_users:
            if db_comm_banned_user.unbanned:
                continue

            self.add(db_comm_banned_user.steamid)

        db_session.close()


class VoiceBannedUserDatabase(CommBannedUserDatabase):
    db_class = DB_VoiceBannedUser

voice_banned_user_database = VoiceBannedUserDatabase()


class TextBannedUserDatabase(CommBannedUserDatabase):
    db_class = DB_TextBannedUser

text_banned_user_database = TextBannedUserDatabase()


@Event('player_spawn')
def on_player_spawn(game_event):
    client = client_manager.from_userid(game_event['userid'])

    if client.player.steamid in voice_banned_user_database:
        tell(client, strings_module['voice_ban_notification'])

    if client.player.steamid in text_banned_user_database:
        tell(client, strings_module['text_ban_notification'])


@InternalEvent('load')
def on_load(event_var):
    voice_banned_user_database.refresh()
    text_banned_user_database.refresh()


import os

from .. import parse_modules


__all__ = parse_modules(os.path.dirname(__file__))

from . import *
