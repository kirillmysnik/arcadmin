from json import loads

from listeners.tick import GameThread

from ...classes.auth import auth_manager, BaseAuthProvider, Permission
from ...models.admin import Admin as DB_Admin
from ...resource.sqlalchemy import Session


class Admin:
    steamid = ""
    permissions = None


class SQLAdminManager(dict):
    def refresh(self):
        self.clear()

        db_session = Session()

        db_admins = db_session.query(DB_Admin).all()

        for db_admin in db_admins:
            if not db_admin.active:
                continue

            admin = Admin()
            admin.steamid = db_admin.steamid
            admin.permissions = set()
            for flag, level in loads(db_admin.permissions).items():
                admin.permissions.add(Permission(flag, level))

            self[admin.steamid] = admin

        db_session.close()

sql_admin_manager = SQLAdminManager()


class SQLAuthProvider(BaseAuthProvider):
    def get_permission_level(self, client, flag):
        if client.player.steamid not in sql_admin_manager:
            return -1

        admin = sql_admin_manager[client.player.steamid]

        max_ = -1
        for permission in admin.permissions:
            if permission.matches(flag):
                max_ = max(max_, permission.level)

        return max_

    def reload_admins(self):
        GameThread(target=sql_admin_manager.refresh).start()

sql_auth_provider = SQLAuthProvider()
auth_manager.add_provider('sql', sql_auth_provider)
