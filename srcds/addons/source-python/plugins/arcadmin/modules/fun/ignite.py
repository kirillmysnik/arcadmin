from events import Event
from listeners import OnClientDisconnect
from listeners.tick import Delay

from ...classes.menu import PlayerBasedCommand
from ...resource.strings import build_module_strings

from . import section


IGNITE_TIME = 5.0


strings_module = build_module_strings('fun.ignite')
burning_players = {}


def extinguish(client, player):
    if player.index not in burning_players:
        return

    player.ignite_lifetime(0)
    del burning_players[player.index]


def toggle_ignite(client, player):
    if player.index in burning_players:
        burning_players[player.index].cancel()
        extinguish(client, player)

        client.announce(strings_module['extinguished'].tokenize(
            player=player.name,
        ))

    else:
        player.ignite()
        burning_players[player.index] = Delay(
            IGNITE_TIME, extinguish, client, player)

        client.announce(strings_module['ignited'].tokenize(
            player=player.name,
            time=IGNITE_TIME
        ))


class IgniteCommand(PlayerBasedCommand):
    base_filter = 'alive'
    include_equal_priorities = True

    @staticmethod
    def player_name(player):
        if player.index in burning_players:
            return strings_module['popup prefix'].tokenize(base=player.name)
        return player.name

    def player_select_callback(self, client, players):
        for player in players:
            toggle_ignite(client, player)

section.add_child(
    IgniteCommand, strings_module['popup title'], 'admin.fun.ignite', 'ignite')


@Event('round_start')
def on_round_start(game_event):
    burning_players.clear()


@OnClientDisconnect
def listener_on_client_disconnect(index):
    if index in burning_players:
        burning_players.pop(index).cancel()
