from colors import Color, WHITE
from engines.sound import Sound
from events import Event
from listeners import OnClientDisconnect

from ...classes.menu import PlayerBasedCommand
from ...resource.strings import build_module_strings

from . import section


FREEZE_COLOR = Color(60, 138, 255, 240)
UNFREEZE_COLOR = WHITE

FREEZE_SOUND_PATH = r"physics\glass\glass_impact_bullet1.wav"
UNFREEZE_SOUND_PATH = r"physics\glass\glass_impact_bullet4.wav"


strings_module = build_module_strings('fun.freeze')
frozen_players = set()


def toggle_freeze(client, player):
    if player.index in frozen_players:
        player.stuck = False
        frozen_players.remove(player.index)

        player.color = UNFREEZE_COLOR

        Sound(UNFREEZE_SOUND_PATH, index=player.index).play()

        client.announce(strings_module['unfrozen'].tokenize(
            player=player.name,
        ))

    else:
        player.stuck = True
        frozen_players.add(player.index)

        player.color = FREEZE_COLOR

        Sound(FREEZE_SOUND_PATH, index=player.index).play()

        client.announce(strings_module['frozen'].tokenize(
            player=player.name,
        ))


class FreezeCommand(PlayerBasedCommand):
    base_filter = 'alive'
    include_equal_priorities = True

    @staticmethod
    def player_name(player):
        if player.index in frozen_players:
            return strings_module['popup prefix'].tokenize(base=player.name)
        return player.name

    def player_select_callback(self, client, players):
        for player in players:
            toggle_freeze(client, player)

section.add_child(
    FreezeCommand, strings_module['popup title'], 'admin.fun.freeze', 'freeze')


@Event('round_start')
def on_round_start(game_event):
    frozen_players.clear()


@OnClientDisconnect
def listener_on_client_disconnect(index):
    frozen_players.discard(index)
