from ...resource.strings import build_module_strings

from ...classes.menu import popup_main, Section


strings_module = build_module_strings('life_management')
section = popup_main.add_child(Section, strings_module['popup title'])


import os

from .. import parse_modules


__all__ = parse_modules(os.path.dirname(__file__))

from . import *
