from ...classes.menu import PlayerBasedCommand
from ...resource.strings import build_module_strings

from . import section


VERTICAL_PUSH_FORCE = 1000.0


strings_module = build_module_strings('life_management.resurrect')


class ResurrectCommand(PlayerBasedCommand):
    base_filter = 'dead'
    include_equal_priorities = True

    def player_select_callback(self, client, players):
        for player in players:
            if not player.dead:
                continue

            player.spawn()

            client.announce(strings_module['resurrected'].tokenize(
                player=player.name
            ))

section.add_child(ResurrectCommand, strings_module['popup title'],
                  'admin.life_management.resurrect', 'resurrect')
