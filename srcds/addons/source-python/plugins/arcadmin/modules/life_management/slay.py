from ...classes.menu import PlayerBasedCommand

from ...resource.strings import build_module_strings

from . import section


VERTICAL_PUSH_FORCE = 1000.0


strings_module = build_module_strings('life_management.slay')


class SlayCommand(PlayerBasedCommand):
    base_filter = 'alive'
    include_equal_priorities = True

    def player_select_callback(self, client, players):
        for player in players:
            if player.dead:
                continue

            player.push(0, VERTICAL_PUSH_FORCE, vert_override=True)
            player.take_damage(player.health + 1)

            client.announce(strings_module['slayed'].tokenize(
                player=player.name
            ))

section.add_child(SlayCommand, strings_module['popup title'],
                  'admin.life_management.slay', 'slay')
