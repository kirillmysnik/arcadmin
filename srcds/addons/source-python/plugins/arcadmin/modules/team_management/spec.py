from players.teams import teams_by_name

from ...classes.menu import PlayerBasedCommand
from ...resource.strings import build_module_strings

from . import section


strings_module = build_module_strings('team_management.spec')


class SpecCommand(PlayerBasedCommand):
    include_equal_priorities = True

    def filter(self, client, player):
        if not super().filter(client, player):
            return False

        # Maybe adjust base_filter instead of doing THIS?
        return player.team in (teams_by_name['t'], teams_by_name['ct'])

    def player_select_callback(self, client, players):
        for player in players:
            player.team = teams_by_name['spec']

            client.announce(strings_module['swapped'].tokenize(
                player=player.name
            ))

section.add_child(SpecCommand, strings_module['popup title'],
                  'admin.team_management.spec', 'spec')
