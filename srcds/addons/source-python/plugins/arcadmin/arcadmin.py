from commands.client import ClientCommand
from commands.say import SayCommand
from commands.server import ServerCommand
from core import echo_console
from filters.players import PlayerIter
from listeners import OnClientActive, OnLevelShutdown

from .classes.auth import auth_manager
from .classes.client import client_manager, broadcast
from .classes.menu import popup_main

from .internal_events import InternalEvent

from .resource.strings import strings_common

from .modules import *

from . import models
from .resource.sqlalchemy import Base, engine
Base.metadata.create_all(engine)


def load():
    # Force dictionary to load all existing players
    for player in PlayerIter():
        client = client_manager[player.index]

    InternalEvent.fire('load')
    broadcast(strings_common['load'])


def unload():
    InternalEvent.fire('unload')
    broadcast(strings_common['unload'])


@OnClientActive
def listener_on_client_active(index):
    # Force dictionary to load a new player
    client = client_manager[index]


@OnLevelShutdown
def listener_on_level_init():
    client_manager.clear()


@ClientCommand('arcadmin')
def client_arcadmin(command, index):
    client = client_manager[index]
    popup_main.show_popup(client)


@SayCommand('!arcadmin')
def say_arcadmin(command, index, teamonly):
    client = client_manager[index]
    popup_main.show_popup(client)


@ServerCommand('arcadmin_reload_admins')
def server_arcadmin_reload_admins(command):
    auth_manager.reload_admins()
    echo_console(
        "Admins are reloaded. Note that some auth providers may take a while "
        "to refresh, they will be empty until they're fully reloaded.")
