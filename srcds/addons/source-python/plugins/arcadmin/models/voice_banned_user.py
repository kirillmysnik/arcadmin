from sqlalchemy import Boolean, Column, Integer, String

from ..resource.config import config
from ..resource.sqlalchemy import Base


class VoiceBannedUser(Base):
    __tablename__ = config['database']['prefix'] + "voice_banned_users"

    id = Column(Integer, primary_key=True)
    steamid = Column(String(32))
    name = Column(String(32))
    admin_steamid = Column(String(32))

    banned_timestamp = Column(Integer)
    unbanned = Column(Boolean)
