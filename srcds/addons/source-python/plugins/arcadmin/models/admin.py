from sqlalchemy import Boolean, Column, Integer, String, Text

from ..resource.config import config
from ..resource.sqlalchemy import Base


class Admin(Base):
    __tablename__ = config['database']['prefix'] + "admins"

    id = Column(Integer, primary_key=True)
    steamid = Column(String(32))
    name = Column(String(32))
    permissions = Column(Text)
    active = Column(Boolean)
