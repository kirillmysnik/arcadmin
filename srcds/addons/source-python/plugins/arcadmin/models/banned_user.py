from sqlalchemy import Boolean, Column, Integer, String, Text

from ..resource.config import config
from ..resource.sqlalchemy import Base


class BannedUser(Base):
    __tablename__ = config['database']['prefix'] + "banned_users"

    id = Column(Integer, primary_key=True)
    steamid = Column(String(32))
    name = Column(String(32))
    admin_steamid = Column(String(32))
    reviewed = Column(Boolean)

    banned_timestamp = Column(Integer)
    expires_timestamp = Column(Integer)

    unbanned = Column(Boolean)

    reason = Column(Text)
    notes = Column(Text)
