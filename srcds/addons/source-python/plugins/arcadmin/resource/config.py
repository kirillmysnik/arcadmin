from configparser import ConfigParser

from .paths import ARCADMIN_DATA_PATH


CONFIG_FILE = ARCADMIN_DATA_PATH / "config.ini"

config = ConfigParser()
config.read(CONFIG_FILE)
