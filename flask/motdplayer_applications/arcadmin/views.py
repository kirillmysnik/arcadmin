from flask import render_template

from . import app, plugin_instance


MAX_REASON_LEN = 512
MAX_DURATION = 315360000    # 10 years


@app.route(plugin_instance.get_base_authed_route('bans'))
def route_bans(steamid, auth_method, auth_token, session_id):
    context = {
        'server_id': plugin_instance.server_id,
        'stylesheets': ('main', 'bans', 'serverclock', 'status'),
        'scripts': ('dom', 'bans', 'serverclock', 'status'),
        'steamid': steamid,
        'auth_method': auth_method,
        'auth_token': auth_token,
        'session_id': session_id,
    }
    return render_template('arcadmin/route_bans.html', **context)


@plugin_instance.json_authed_request('json-bans')
def route_json_bans(data_exchanger, json_data):
    if 'action' not in json_data:
        return {'error': "VIEW_INVALID_REQUEST"}

    if json_data['action'] == "init":
        return data_exchanger.exchange({
            'action': "init",
        })

    if json_data['action'] == "confirm-ban":
        if (
                "ban_id" not in json_data or
                "duration" not in json_data or
                "reason" not in json_data
        ):

            return {'error': "VIEW_INVALID_REQUEST"}

        reason = json_data['reason'].strip()
        if not reason:
            return {'error': "VIEW_INVALID_REQUEST"}

        try:
            duration = int(json_data['duration'])
            if duration > MAX_DURATION or duration < -1:
                raise ValueError
        except ValueError:
            return {'error': "VIEW_INVALID_REQUEST"}

        return data_exchanger.exchange({
            'action': "confirm-ban",
            'ban_id': json_data['ban_id'],
            'duration': duration,
            'reason': reason,
        })

    if json_data['action'] == "unban":
        if "ban_id" not in json_data:
            return {'error': "VIEW_INVALID_REQUEST"}

        return data_exchanger.exchange({
            'action': "unban",
            'ban_id': json_data['ban_id'],
        })

    return {'error': "VIEW_INVALID_REQUEST"}
